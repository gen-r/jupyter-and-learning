# Jupyter and learning

Information node on the use of Jupyter Notebooks in higher education learning

## Jupyter Notebooks Learning Resources

Links pad

Thanks to: de-RSE; Open Science Lab - TIB;

Zotero references https://www.zotero.org/groups/1838445/generation_r/items/tag/Jupyter%20notebooks

### Please add links here and I'll sort them later!!!

> 

> add here https://github.com/spors

### Learning

Text Processing - tutorial – https://github.com/awagner-mainz/notebooks/blob/master/gallery/TextProcessing_Solorzano.ipynb | Andreas Wagner, Digital Humanities, Max-Planck-Institute for European Legal History https://www.rg.mpg.de/

Introduction to Python for Computational Science and Engineering – https://github.com/fangohr/introduction-to-python-for-computational-science-and-engineering/blob/master/Readme.md | developed by Hans Fangohr (2003-2018)

‘Generating Software Tests: Breaking Software for Fun and Profit’ – https://www.fuzzingbook.org/ – Chapters on software testing released every week since 2018-10-30, by Andreas Zeller, Rahul Gopinath, Marcel Böhme, Gordon Fraser, and Christian Holler

Lab for "Machine Learning for Text Analysis" Daniel Speicher 

Latent Dirichlet Allocation

Announcement: https://www.researchgate.net/project/P3ML-ML-Engineering-Knowledge/update/5c4f789c3843b0544e62df38

Example: https://mybinder.org/v2/gh/p3ml/latent_dirichlet_allocation/master?filepath=LDA%20on%20Returns.ipynb 

An example of LDA for libraries (although not Jupyter based, this serves as an example of LDA)
https://www.hypershelf.org/sep/80/?doc=turing-machine 

Executable NumPy / SciPy Recipes collection Christian Bauckhage

The entry page for all these notebooks is https://p3ml.github.io/

Binder link https://mybinder.org/v2/gh/p3ml/recipes/master


##### Example of documentation in Jupyter

Finite Element Solver – https://ngsolve.org/docu/latest/index.html


#### Dev issues in learning

Reviewing, commenting https://github.com/ReviewNB/support | https://github.com/ReviewNB/support/issues/1 


### Technical, how to
OUseful.Info – https://blog.ouseful.info/ |  open education and data journalism | Tony Hirst
@psychemedia

https://www.gw-openscience.org/tutorials/ 

Gesis - binder hosting https://notebooks.gesis.org/

### Literature

Rule, Adam, Aurélien Tabard, and James D. Hollan. ‘Exploration and Explanation in Computational Notebooks’. CHI ’18 Proceedings of the 2018 CHI Conference on Human Factors in Computing Systems, 2018. https://doi.org/10.1145/3173574.3173606.

### News 

JupyterHub has a "meta" repository https://github.com/jupyterhub/team-compass

### Technologies

MyBinder https://mybinder.org/

### Support companies

Hub Hero https://hubhero.net/

### Unsorted

Questions about Jupyter https://docs.google.com/presentation/d/1n2RlMdmv1p25Xy5thJUhkKGvjtV-dkAIsUXP-AL4ffI/edit#slide=id.g362da58057_0_1

Thread for teaching examples https://carpentries.topicbox.com/groups/discuss/T1505f74d7f6e32f8-M84f62eac740739ea0a0f32af/slide-of-joel-grus-jupytercon-talk-i-dont-like-notebooks

Bash backend

